#include <stdio.h>

__attribute__((constructor))
static void init() {
  printf("Inner library constructed!\n");
}

void inner() {
  printf("Inside inner!\n");
}
