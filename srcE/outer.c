#include <stdio.h>
#include <dlfcn.h>

typedef void (*PFN_inner)();

static PFN_inner inner;

__attribute__((constructor))
static void init() {
  printf("Loading inner library!\n");
  void* lib = dlopen(INNER_LIB, RTLD_NOW);
  inner = dlsym(lib, "inner");
}

int main() {
  printf("Calling down into inner...\n");
  inner();
  return 0;
}
