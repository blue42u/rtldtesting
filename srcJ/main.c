#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include <dlfcn.h>

struct state {
  pthread_barrier_t* bar;
  const char* bin;
};

static void* body(void* vp_state) {
  struct state* state = vp_state;

  // Wait at the barrier to line up with everyone else
  pthread_barrier_wait(state->bar);

  // Try to pull in a new library into the link map
  void* handle = dlopen(state->bin, RTLD_LAZY | RTLD_LOCAL);

  // Load in the symbol and call it
  void (*foo)() = dlsym(handle, "foo");
  foo();

  // Free it up, we're done here.
  dlclose(handle);
  return NULL;
}

int main() {
  // Set up a barrier between the 3 threads.
  pthread_barrier_t bar;
  pthread_barrier_init(&bar, NULL, 3);

  // Each thread has a slightly different internal state.
  struct state state[3] = {{&bar, "libJ1.so"}, {&bar, "libJ2.so"}, {&bar, "libJ3.so"}};

  // Create a bunch of threads, so we can cause some races
  pthread_t threads[3];
  for(size_t i = 0; i < 3; i++) pthread_create(&threads[i], NULL, body, &state[i]);

  // Wait for the threads to finish up their business
  for(size_t i = 0; i < 3; i++) pthread_join(threads[i], NULL);

  pthread_barrier_destroy(&bar);
  return 0;
}
