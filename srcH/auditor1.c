#define _GNU_SOURCE
#include <link.h>
#include <stdio.h>

unsigned int la_version(unsigned int ver) {
  printf("[H1] Auditor loaded!\n");
  return ver;
}

void la_activity(uintptr_t* cookie, unsigned int flag) {
  if(flag == LA_ACT_ADD)
    printf("[H1] Linkmap update: LA_ACT_ADD\n");
  else if(flag == LA_ACT_DELETE)
    printf("[H1] Linkmap update: LA_ACT_DELETE\n");
  else if(flag == LA_ACT_CONSISTENT)
    printf("[H1] Linkmap update: LA_ACT_CONSISTENT\n");
}

unsigned int la_objopen(struct link_map* map, Lmid_t lmid, uintptr_t* cookie) {
  printf("[H1] Library open `%s'\n", map->l_name);
  return 0;
}
