#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
  printf("Spawning inner process (echo)...\n");
  if(fork() == 0) {
    execl("/bin/echo", "echo", "Within inner echo!", NULL);
  } else wait(NULL);
  printf("Process complete, closing up...\n");
  return 0;
}
