#include <stdio.h>
#include <dlfcn.h>

typedef void (*PFN_inner)();

int main() {
  printf("Loading inner library " INNER_LIB "...\n");
  void* lib = dlopen(INNER_LIB, RTLD_NOW);
  printf("Looking up function...\n");
  PFN_inner inner = dlsym(lib, "middle");
  printf("Calling down to %p...\n", inner);
  inner();
  printf("Closing up...\n");
  dlclose(lib);
}
