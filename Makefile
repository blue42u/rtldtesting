.PHONY: all runA runB runC runD runE runF runH runI runJ
all: runA runB runC runD runE runF runH runI runJ

libtest.so: src/testlib.c src/testapi.h
	gcc -shared -o $@ $^ -fPIC
libtestaudit.so: src/testaudit.c src/testapi.h
	gcc -shared -o $@ $^ -fPIC -ldl `pwd`/libtest.so
.PHONY: testbits
testbits: testrun.sh libtest.so libtestaudit.so

srcA/libA1.so: srcA/inner1.c
	gcc -shared -o $@ $< -fPIC
srcA/libA2.so: srcA/inner2.c srcA/libA1.so
	gcc -shared -o $@ $< -fPIC "`pwd`"/srcA/libA1.so
testA: srcA/outer.c srcA/libA2.so srcA/libA1.so
	gcc -o $@ $< "`pwd`"/srcA/libA2.so "`pwd`"/srcA/libA1.so
runA: testA testbits
	@echo '# A. Libraries loaded via linker flags'
	@./testrun.sh ./testA
	@echo

testB: srcB/outer.c srcA/libA2.so
	gcc -o $@ $< -DINNER_LIB=\""`pwd`"/srcA/libA2.so\" -ldl
runB: testB testbits
	@echo '# B. Library subtree loaded via dlopen'
	@./testrun.sh ./testB
	@echo

testC: srcC/outer.c
	gcc -o $@ $<
runC: testC testbits
	@echo '# C. Subprocess execution'
	@./testrun.sh ./testC
	@echo

srcD/inner: srcD/inner.c
	gcc -o $@ $<
	chmod a-r $@
testD: srcD/outer.c srcD/inner
	gcc -o $@ $< -DINNER_BIN=\""`realpath srcD/inner`"\"
runD: testD testbits
	@echo '# D. Unreadable subprocess execution'
	@./testrun.sh ./testD
	@echo

srcE/libE.so: srcE/inner.c
	gcc -shared -o $@ $< -fPIC
testE: srcE/outer.c srcE/libE.so
	gcc -o $@ $< -DINNER_LIB=\""`realpath srcE/libE.so`"\" -ldl
runE: testE testbits
	@echo '# E. Call to dlopen in constructor'
	@./testrun.sh ./testE
	@echo

srcF/inner.so: srcF/inner.c
	gcc -shared -o $@ $< -fPIC
srcF/middle.so: srcF/middle.c srcF/inner.so
	gcc -shared -o $@ $< -fPIC -DINNER_LIB=\""`realpath srcF/inner.so`"\" -ldl
testF: srcF/outer.c
	gcc -o $@ $<
runF: testF srcF/middle.so testbits
	@echo '# F. Call to dlopen in PRELOADed constructor'
	@XX_LD_PRELOAD="`realpath srcF/middle.so`" ./testrun.sh ./testF
	@echo

srcG/inner.so: srcG/inner.c
	gcc -shared -o $@ $< -fPIC
testG: srcG/outer.c srcG/inner.so
	gcc -o $@ $< -Wl,-rpath,"`realpath srcG/`" -ldl
runG: testG testbits
	@echo "# G. Library dlopen'd via RUNPATH"
	@./testrun.sh ./testG
	@echo

srcH/auditor1.so: srcH/auditor1.c
	gcc -shared -o $@ $< -fPIC
srcH/auditor2.so: srcH/auditor2.c
	gcc -shared -o $@ $< -fPIC
runH: testA srcH/auditor1.so srcH/auditor2.so testbits
	@echo "# H. Secondary audit libraries"
	@XX_LD_AUDIT="`realpath srcH/auditor1.so`:`realpath srcH/auditor2.so`" ./testrun.sh ./testA
	@echo

srcI/auditor.so: srcI/auditor.c
	gcc -shared -o $@ $< -fPIC -lrt
testI: srcI/main.c
	gcc -o $@ $<
runI: testI srcI/auditor.so testbits
	@echo "# I. Auditor-initiated timers"
	@XX_LD_AUDIT="`realpath srcI/auditor.so`" ./testrun.sh ./testI
	@echo

srcJ/libJ1.so: srcJ/lib.c
	gcc -shared -o $@ $< -fPIC -DFOO_ID=1
srcJ/libJ2.so: srcJ/lib.c
	gcc -shared -o $@ $< -fPIC -DFOO_ID=2
srcJ/libJ3.so: srcJ/lib.c
	gcc -shared -o $@ $< -fPIC -DFOO_ID=3
testJ: srcJ/main.c
	gcc -o $@ $< -Wl,-rpath,"`realpath srcJ/`" -ldl -pthread
runJ: testJ srcJ/libJ1.so srcJ/libJ2.so srcJ/libJ3.so testbits
	@echo "# J. dlopen from multiple threads"
	@./testrun.sh ./testJ
	@echo
