#include "testapi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

__attribute__((constructor))
static void testrun_init() {
  printf("[track] Started!\n");
}

__attribute__((destructor))
static void testrun_deinit() {
  printf("[track] Stopped!\n");
}

void testrun_opened(size_t from, size_t to, const char* path, uintptr_t* ud) {
  printf("[track] Range [%zx,%zx) mapped to `%s'\n", from, to, path);
  *ud = (uintptr_t)strdup(path);
}

void testrun_closed(uintptr_t* ud) {
  char* path = (char*)*ud;
  printf("[track] Unmapped `%s'\n", path);
  free(path);
}
