#include <stddef.h>
#include <stdint.h>

// API for the link between the auditor and the PRELOADed part.

#define MAPI_opened(F) \
  void F(opened) (size_t start, size_t end, const char* path, \
                  uintptr_t* userdata)
#define MAPI_closed(F) \
  void F(closed) (uintptr_t* userdata)

#define MAPI_PFN_F(N) (*PFN_##N)
#define MAPI_EXT_F(N) testrun_##N

typedef MAPI_opened(MAPI_PFN_F);
extern MAPI_opened(MAPI_EXT_F);

typedef MAPI_closed(MAPI_PFN_F);
extern MAPI_closed(MAPI_EXT_F);

// Special values for *userdata that should be avoided by the tracker.
#define UD_MISSED  ((uintptr_t)1)  // Untracked library
#define UD_TRACKER ((uintptr_t)2)  // Library for the tracker itself
