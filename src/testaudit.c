#define _GNU_SOURCE
#include "testapi.h"

#include <stdlib.h>
#include <link.h>
#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

static bool connected = false;
static bool trackerstable = false;
static bool disconnected = false;
static PFN_opened opened = NULL;
static PFN_closed closed = NULL;
static char* tracker = NULL;

static void try_connect(struct link_map* map);

unsigned int la_version(unsigned int ver) {
  printf("[audit] Loading auditor, API version %u\n", ver);
  tracker = getenv("TESTRUN_LIBRARY");
  if(tracker == NULL) {
    printf("[audit] TESTRUN_LIBRARY not set in the environment, disabling auditing.\n");
    return 0;
  }
  printf("[audit] Awaiting tracker `%s'\n", tracker);
  tracker = strdup(tracker);  // In case someone uses setenv
  return ver;
}

void la_activity(uintptr_t* cookie, unsigned int flag) {
  if(flag == LA_ACT_ADD)
    printf("[audit] Link map unstable: adding entries\n");
  else if(flag == LA_ACT_DELETE)
    printf("[audit] Link map unstable: removing entries\n");
  else if(flag == LA_ACT_CONSISTENT)
    printf("[audit] Link map stablized\n");

  if(!trackerstable && connected && !disconnected) {
    if(flag == LA_ACT_CONSISTENT) {
      printf("[audit] Connection to tracker stablized\n");
      trackerstable = true;
    }
  }
}

unsigned int la_objopen(struct link_map* map, Lmid_t lmid, uintptr_t* cookie) {
  // Get the file size from stat(). According to the man page, this will
  // work even without read permission (but it does need exec on the path).
  struct stat st;
  if(stat(map->l_name, &st) != 0) {
    int e = errno;
    char buf[1024];
    const char* estr = strerror_r(e, buf, sizeof buf);
    printf("[audit] Failed to stat `%s': %s\n", map->l_name, estr);
    *cookie = UD_MISSED;
    return 0;
  }

  // Make sure we're connected before going too far.
  if(!connected) {
    try_connect(map);
    if(connected) {
      printf("[audit] Found tracker library, mapped at [%zx,%zx)\n",
        map->l_addr, map->l_addr + st.st_size);
      *cookie = UD_TRACKER;
      return 0;
    } else {
      printf("[audit] Open before connection: [%zx,%zx) -> `%s'\n",
        map->l_addr, map->l_addr + st.st_size, map->l_name);
      *cookie = UD_MISSED;
      return 0;
    }
  }

  // If the link map hasn't stablized since connection, we need to give
  // it a moment. Otherwise nothing's relocated yet.
  if(!trackerstable) {
    printf("[audit] Open before stablization: [%zx,%zx) -> `%s'\n",
      map->l_addr, map->l_addr + st.st_size, map->l_name);
    *cookie = UD_MISSED;
    return 0;
  }

  // We *might* have had our connection cut by this point. Really shouldn't
  // happen, but technically possible.
  if(disconnected) {
    printf("[audit] Open after disconnection: [%zx,%zx) -> `%s'\n",
      map->l_addr, map->l_addr + st.st_size, map->l_name);
    *cookie = UD_MISSED;
    return 0;
  }

  // Figure out the ranges and notify the tracker.
  opened(map->l_addr, map->l_addr + st.st_size, map->l_name, cookie);
  return 0;
}

unsigned int la_objclose(uintptr_t* cookie) {
  if(*cookie == UD_MISSED) {
    //printf("[audit] Missing close: missed open\n");
  } else if(*cookie == UD_TRACKER) {
    printf("[audit] Tracker disconnected\n");
    disconnected = true;
    free(tracker);
  } else if(!connected || disconnected) {
    printf("[audit] Skipping close after disconnection\n");
  } else {
    closed(cookie);
  }
  return 0;
}

static void try_connect(struct link_map* map) {
  // Check if the path is what we expect for the tracker
  if(strcmp(map->l_name, tracker) != 0) return;

  // Nab the STRTAB and SYMTAB
  const char* strtab = NULL;
  ElfW(Sym)* symtab = NULL;
  for(const ElfW(Dyn)* d = map->l_ld; d->d_tag != DT_NULL; d++) {
    if(d->d_tag == DT_STRTAB) strtab = (const char*)d->d_un.d_ptr;
    else if(d->d_tag == DT_SYMTAB) symtab = (ElfW(Sym)*)d->d_un.d_ptr;
    if(strtab && symtab) break;
  }
  if(!strtab || !symtab) return;  // Probably a VERY bad thing, but hey.

  // Now scan for our tracker's hooks
  for(ElfW(Sym)* s = symtab; !opened || !closed; s++) {
    if(ELF64_ST_TYPE(s->st_info) != STT_FUNC) continue;
    if(strcmp(&strtab[s->st_name], "testrun_opened") == 0) {
      opened = (PFN_opened)(map->l_addr + s->st_value);
    } else if(strcmp(&strtab[s->st_name], "testrun_closed") == 0) {
      closed = (PFN_closed)(map->l_addr + s->st_value);
    }
  }

  // Mark that we have connected
  connected = true;
}
