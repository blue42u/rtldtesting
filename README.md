## LD_AUDIT Study

Just a simple test for using LD_AUDIT. Compile with `make`, which also
runs all the examples.

Run individual examples with `make run{A,B,C,D,E,F,G}`.

Also includes a helper script for running commands, use
`.../testrun.sh command...` to test it on other things.

### Approach Caveats (I'm a pessimist)
 1. You can't `LD_PRELOAD` and `LD_AUDIT` the same library, so you need at least
    two libraries for it to work. In practice, the "auditor" just calls hooks
    in the PRELOADed "tracker."

 2. The "auditor" is loaded in a namespace separate from the rest of the
    program, so obtaining said hooks is tricky. `dlsym(RTLD_DEFAULT)` doesn't
    work, nor does `dlopen` or linking (you end up with two copies in the
    address space).

    The solution implemented here uses the wrapper script to side-channel
    the name of the "tracker" library, which is then matched on open and
    the hook symbols are extracted manually from its symbol table (with the
    minor detail that you have to wait for the link map to be `CONSISTENT`
    before using them).

 3. The "auditor" is loaded long (long!) before the application, so the
    first moment the "auditor" is able to use the hooks is before the
    constructor of the "tracker." In practice this shouldn't be too big
    a deal, the "auditor" could call a separate hook on the first go.

 4. Adding to that, the "auditor" is initalized before ld.so is completely
    prepared, so `dlopen` and `dlsym` don't work until the link map
    becomes `CONSISTENT` for the first time. Even after that, the namespace
    separation means they don't give useful results, so if you want to use
    a symbol it'll have to be gotten by the "tracker."

 5. You can't use `gdb` or `valgrind` to test when things go south in the
    "auditor," the only debugging is by `printf`. In practice, the "auditor"
    should be minimized as much as possible, so this might be viable.

 6. Auditors can only see other auditors if they appear "after" them in the
    `LD_AUDIT` list, which means signals may interupt locations with no link
    map notifications. A simple workaround is to ensure the "auditor" is always
    at the head of the list, and ignore addresses that don't appear to be mapped.

### Approach Benefits
 1. There's an audit event every time the linkmap changes, no matter what
    caused it (linker flags or `dlopen`). For example, from `make runB`:
```
# B. Library subtree loaded via dlopen
...snip...
[audit] Open before connection: [7f1f0ec3d000,7f1f0ec666f8) -> `/lib64/ld-linux-x86-64.so.2'
[audit] Link map unstable: adding entries
[audit] Found tracker library, mapped at [7f1f0ec31000,7f1f0ec34f80)
[audit] Open before stablization: [7f1f0ec2c000,7f1f0ec2f900) -> `/lib/x86_64-linux-gnu/libdl.so.2'
[audit] Open before stablization: [7f1f0e72c000,7f1f0e8eb2b0) -> `/lib/x86_64-linux-gnu/libc.so.6'
[audit] Link map stablized
[audit] Connection to tracker stablized
[track] Started!
Loading inner library /home/deepthought/rtldtest/srcA/libA2.so...
[audit] Link map unstable: adding entries
[track] Range [7f1f0ec25000,7f1f0ec28e90) mapped to `.../rtldtest/srcA/libA2.so'
[track] Range [7f1f0ec20000,7f1f0ec23e68) mapped to `.../rtldtest/srcA/libA1.so'
[audit] Link map stablized
...snip...
Closing up...
[track] Unmapped `/home/deepthought/rtldtest/srcA/libA2.so'
[track] Unmapped `/home/deepthought/rtldtest/srcA/libA1.so'
...snip...
```

 2. Similar to `LD_PRELOAD`, `LD_AUDIT` will propagate to subprocesses through the
    environment (except in secure-execution mode, as expected). It doesn't
    matter whether the executable is readable or not. From `make runD`:
```
# D. Unreadable subprocess execution
[audit] Loading auditor, API version 1
...snip...
Spawning inner process...
[audit] Loading auditor, API version 1
...snip...
Super secret inner process!
[track] Stopped!
[audit] Tracker disconnected
Process complete, closing up...
[track] Stopped!
[audit] Tracker disconnected
```

  3. There are auditing events for when the linkmap is unstable, which appear to
     be the moments at which `dl*` is problematic. Without wrapping `dlopen`, we
     can track the *exact* moments when we can't call `dl_iterate_phdr`, which
     happens to *not* include the constructors themselves. From `make runE`:
```
# E. Call to dlopen in constructor
...snip...
[track] Started!
Loading inner library!
[audit] Link map unstable: adding entries
[track] Range [7f28d344d000,7f28d3450e80) mapped to `/home/deepthought/rtldtest/srcE/libE.so'
[audit] Link map stablized
Inner library constructed!
Calling down into inner...
Inside inner!
...snip...
```

  4. Since `LD_AUDIT` is started early, it also gets auditing events caused by
     PRELOADed libraries, no matter the PRELOAD order. From `make runF`:
```
# F. Call to dlopen in PRELOADed constructor
...snip...
[audit] Connection to tracker stablized
Loading inner library!
[audit] Link map unstable: adding entries
[track] Range [7f76cd47b000,7f76cd47ee80) mapped to `/home/deepthought/rtldtest/srcF/inner.so'
[audit] Link map stablized
Inner library constructed!
Calling down into inner...
Inside inner!
Cleaning up...
[track] Unmapped `/home/deepthought/rtldtest/srcF/inner.so'
[audit] Link map unstable: removing entries
[audit] Link map stablized
[track] Started!
Outer application run!
[track] Stopped!
[audit] Tracker disconnected
```

 5. As expected, the auditing events also act on RUNPATH paths and work
    exactly like the original program. From `make runG`:
```
# G. Library dlopen'd via RUNPATH
...snip...
Loading inner library...
[audit] Link map unstable: adding entries
[track] Range [7f3380b71000,7f3380b74e68) mapped to `srcG/inner.so'
[audit] Link map stablized
Calling down into inner...
Cleaning up...
[track] Unmapped `srcG/inner.so'
...snip...
```

 6. Auditors can see other auditors if they are listed later. From `make runH`:
```
# H. Secondary audit libraries
[audit] Loading auditor, API version 1
[audit] Awaiting tracker `/home/deepthought/rtldtest/libtest.so'
[audit] Open before connection: [7fe9bf3fc000,7fe9bf3ffef0) -> `/home/deepthought/rtldtest/srcH/auditor1.so'
[audit] Open before connection: [7fe9beef3000,7fe9bf0b22b0) -> `/lib/x86_64-linux-gnu/libc.so.6'
[audit] Link map stablized
[H1] Auditor loaded!
[audit] Open before connection: [7fe9bf3f5000,7fe9bf3f8ef0) -> `/home/deepthought/rtldtest/srcH/auditor2.so'
[H1] Library open `/home/deepthought/rtldtest/srcH/auditor2.so'
[audit] Open before connection: [7fe9bec30000,7fe9bedef2b0) -> `/lib/x86_64-linux-gnu/libc.so.6'
[H1] Library open `/lib/x86_64-linux-gnu/libc.so.6'
[audit] Link map stablized
[H1] Linkmap update: LA_ACT_CONSISTENT
[H2] Auditor loaded!
[audit] Failed to stat `': No such file or directory
[H1] Library open `'
[H2] Library open `'
[audit] Open before connection: [7fe9bf408000,7fe9bf4316f8) -> `/lib64/ld-linux-x86-64.so.2'
[H1] Library open `/lib64/ld-linux-x86-64.so.2'
[H2] Library open `/lib64/ld-linux-x86-64.so.2'
...snip...
```

 7. Auditors are able to initiate timers as early as they get, which can be seen
    from the application via signals (as usual). For whatever reason, the
    auditor does have to register in order for this to work, otherwise `ld.so`
    fails a namespace consistency assertion. From `make runI`:
```
# I. Auditor-initiated timers
...snip...
[I] Arming timer A!
[I] Arming timer B!
[I] Timer A expired!
...snip...
Main entered, sleeping for a second...
Timer B expired!
[I] Timer A expired!
Timer B expired!
[I] Timer A expired!
Sleep complete! Exiting...
...snip...
```

 8. Notifications are given to auditors in a linear stream, with no overlaps
    between link map destabilization regions. From `make runJ`:
```
# J. dlopen from multiple threads
...snip..
[track] Started!
[audit] Link map unstable: adding entries
[track] Range [7f8e946d6000,7f8e946d9e68) mapped to `/home/deepthought/rtldtest/srcJ/libJ3.so'
[audit] Link map stablized
Foo 3!
[audit] Link map unstable: adding entries
[track] Range [7f8e946d1000,7f8e946d4e68) mapped to `/home/deepthought/rtldtest/srcJ/libJ2.so'
[audit] Link map stablized
Foo 2!
[track] Unmapped `/home/deepthought/rtldtest/srcJ/libJ2.so'
[audit] Link map unstable: removing entries
[audit] Link map stablized
[track] Unmapped `/home/deepthought/rtldtest/srcJ/libJ3.so'
[audit] Link map unstable: removing entries
[audit] Link map stablized
[audit] Link map unstable: adding entries
[track] Range [7f8e946d6000,7f8e946d9e68) mapped to `/home/deepthought/rtldtest/srcJ/libJ1.so'
[audit] Link map stablized
Foo 1!
[track] Unmapped `/home/deepthought/rtldtest/srcJ/libJ1.so'
[audit] Link map unstable: removing entries
[audit] Link map stablized
...snip...
```
