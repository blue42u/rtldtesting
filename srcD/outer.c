#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
  printf("Spawning inner process...\n");
  if(fork() == 0) {
    execl(INNER_BIN, "inner", NULL);
  } else wait(NULL);
  printf("Process complete, closing up...\n");
  return 0;
}
