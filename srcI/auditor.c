#define _GNU_SOURCE
#include <stdio.h>
#include <link.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>

static void notify(int sig) {
  printf("[I] Timer A expired!\n");
}

unsigned int la_version(unsigned int ver) {
  // Set up two timers to shoot ourselves in the foot every so often.
  struct sigevent se = {
    .sigev_notify = SIGEV_SIGNAL,
    .sigev_signo = SIGRTMIN,
  };
  timer_t timerA, timerB;
  timer_create(CLOCK_MONOTONIC, &se, &timerA);
  se.sigev_signo = SIGRTMIN+1;
  timer_create(CLOCK_MONOTONIC, &se, &timerB);

  // Set the signal handler for timer A, the application handles timer B
  struct sigaction sa = { .sa_handler = notify, };
  sigaction(SIGRTMIN, &sa, NULL);

  // Arm timer A, having it trigger basically right now (and then every 200ms after)
  printf("[I] Arming timer A!\n");
  struct itimerspec itsA = {{0,200000000}, {0,1}};
  timer_settime(timerA, 0, &itsA, NULL);

  // Arm timer B, having it trigger in 200ms to let ld.so initialize (and then every 200ms after)
  printf("[I] Arming timer B!\n");
  struct itimerspec itsB = {{0,200000000}, {0,200000000}};
  timer_settime(timerB, 0, &itsB, NULL);

  // We don't actually audit, but we do need to register so our namespace doesn't vaporize.
  return ver;
}
