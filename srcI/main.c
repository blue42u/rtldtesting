#include <time.h>
#include <stdio.h>
#include <signal.h>

static void notify(int sig) {
  printf("Timer B expired!\n");
}

int main() {
  // Set up the sigaction so we can catch triggers from the auditor.
  struct sigaction sa = {
    .sa_handler = notify,
  };
  sigaction(SIGRTMIN+1, &sa, NULL);

  printf("Main entered, sleeping for a second...\n");
  struct timespec ts = {0, 500000000};
  while(nanosleep(&ts, &ts));
  printf("Sleep complete! Exiting...\n");
  return 0;
}
