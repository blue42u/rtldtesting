#include <stdio.h>
#include <dlfcn.h>

typedef void (*PFN_inner)();

__attribute__((constructor))
static void init() {
  printf("Loading inner library!\n");
  void* lib = dlopen(INNER_LIB, RTLD_NOW);
  PFN_inner inner = dlsym(lib, "inner");
  printf("Calling down into inner...\n");
  inner();
  printf("Cleaning up...\n");
  dlclose(lib);
}
