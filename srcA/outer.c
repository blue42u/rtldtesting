#include <stdio.h>

void inner();
void middle();

int main() {
  printf("Going to call the middle library!\n");
  middle();
  printf("Going to call the inner library!\n");
  inner();
  printf("Back in the outer binary!\n");
  return 0;
}
