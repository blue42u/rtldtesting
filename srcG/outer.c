#include <stdio.h>
#include <dlfcn.h>

typedef void (*PFN_inner)();

int main() {
  printf("Loading inner library...\n");
  void* lib = dlopen("inner.so", RTLD_NOW);
  printf("Calling down into inner...\n");
  PFN_inner inner = dlsym(lib, "inner");
  printf("Cleaning up...\n");
  dlclose(lib);
  return 0;
}
