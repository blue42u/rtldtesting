#!/bin/bash

HERE="`dirname "$0"`"

TRACKER="`realpath "$HERE"/libtest.so`"
AUDITOR="`realpath "$HERE"/libtestaudit.so`"

LD_PRELOAD="$TRACKER":"$XX_LD_PRELOAD" LD_AUDIT="$AUDITOR":"$XX_LD_AUDIT" TESTRUN_LIBRARY="$TRACKER" \
exec "$@"
